package cn.wgx.study.moshi.singleton;

/**
 * 枚举 实现单例模式
 */
public enum Singleton {

    INSTANCE;

    private double r = Math.random();


    public void print(){
        System.out.println("this is singleton" + r);
    }

    public static Singleton getInstance(){
        return INSTANCE;
    }



}
