package cn.wgx.study.moshi.singleton;

public class TestSingleton {

    //单例模式
    public static void main(String[] args) {
        Singleton.getInstance().getInstance();
    }

}
