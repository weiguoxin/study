package cn.wgx.study.moshi.factory;

import cn.wgx.study.moshi.strategy.Cat;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 工厂模式 内涵单例模式实现
 */
public class CatFactory {

    private static Cat cat;

    //计数器
    private static AtomicInteger atomicInteger;

    static {
        atomicInteger = new AtomicInteger();
    }

    private CatFactory(){}

    //生产一个新猫
    public static Cat getNewCat(){
        int x = atomicInteger.incrementAndGet();
        System.out.println(x);
        return new Cat(x,x);
    }

    //单例猫
    public static Cat getCat(){
        if(null == cat){
            //双重判断
            synchronized (CatFactory.class){
                if(null == cat){
                    cat = new Cat(2,2);
                }
            }
        }
        return  cat;
    }

}
