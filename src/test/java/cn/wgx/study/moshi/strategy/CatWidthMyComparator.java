package cn.wgx.study.moshi.strategy;


public class CatWidthMyComparator extends MyComparator<Cat> {

    @Override
    public int compare(Cat t1, Cat t2) {
//        if(t1.getWidth() > t2.getWidth()){
//            return 1;
//        }else if(t1.getWidth() == t2.getWidth()){
//            return 0;
//        }
//        return -1;
        return t1.getWidth() - t2.getWidth();
    }

//    @Override
//    public int compareTo(Cat o) {
//        return 0;
//    }
}
