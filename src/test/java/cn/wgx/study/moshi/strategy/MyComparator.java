package cn.wgx.study.moshi.strategy;

import java.util.Comparator;

public abstract class MyComparator<T> implements Comparator<T> {

    //int compare(T t1, T t2);

    public void sort(T ts[]){
        int len = ts.length;
        for (int i=0;i< len-1;i++) {
            for(int j=i;j<len;j++){
                if(this.compare(ts[i],ts[j])>0){
                    T temp = ts[i];
                    ts[i] = ts[j];
                    ts[j] = temp;
                    //swap(ts, i, j);
                }
            }
        }
    }

//    default  void swap(T t[], int i, int j){
//        T temp = t[i];
//        t[i] = t[j];
//        t[j] = temp;
//    }
}
