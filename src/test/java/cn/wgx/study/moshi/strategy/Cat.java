package cn.wgx.study.moshi.strategy;

import lombok.Data;


@Data
public class Cat {

    private int width;

    private int height;


    public Cat(int width, int height){
        this.width = width;
        this.height = height;
    }

}
