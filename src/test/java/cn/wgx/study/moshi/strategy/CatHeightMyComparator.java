package cn.wgx.study.moshi.strategy;

public class CatHeightMyComparator extends MyComparator<Cat> {

    @Override
    public int compare(Cat t1, Cat t2) {
//        if(t1.getHeight() > t2.getHeight()){
//            return 1;
//        }else if(t1.getHeight() == t2.getHeight()){
//            return 0;
//        }
//        return -1;
        return t1.getHeight() - t2.getHeight();
    }

}
