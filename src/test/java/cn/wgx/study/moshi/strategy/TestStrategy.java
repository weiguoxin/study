package cn.wgx.study.moshi.strategy;

import java.util.Arrays;

/**
 * 策略模式
 */
public class TestStrategy {

    public static void main(String[] args) {
        Cat [] cats = {new Cat(1,5), new Cat(2,4),
                new Cat(3,3), new Cat(4,2), new Cat(5,1)};

        //自定义排序方法
        //根据体重排序
        new CatWidthMyComparator().sort(cats);
        System.out.println(Arrays.toString(cats));

        //根据身高排序
        new CatHeightMyComparator().sort(cats);
        System.out.println(Arrays.toString(cats));

        Arrays.sort(cats, new CatWidthMyComparator());
        System.out.println(Arrays.toString(cats));

        Arrays.sort(cats, new CatHeightMyComparator());
        System.out.println(Arrays.toString(cats));


    }


}
