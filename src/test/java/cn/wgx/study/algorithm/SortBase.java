package cn.wgx.study.algorithm;

import java.util.Arrays;
import java.util.Random;

public class SortBase {

    public static int[] sort(int array[]){
        return array;
    }

    //交换位置
    protected static void exchange(int array[], int a, int b) {
        int tmp = array[a];
        array[a] = array[b];
        array[b] = tmp;
    }

    //打印数组
    protected static void print(int[] array) {
//        (array[1])
        System.out.println(Arrays.toString(array));
    }


    //生成数组
    protected static int[] generate(int n, int max) {
        int[] arr = new int[n];
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            arr[i] = r.nextInt(max);
        }
        return arr;
    }

    protected static boolean compare(int[] n, int[] m) {
        if (n.length != m.length) {
            return false;
        }
        for (int i = 0, j = n.length; i < j; i++) {
            if (n[i] != m[i]) {
                return false;
            }
        }
        return true;

    }
}
