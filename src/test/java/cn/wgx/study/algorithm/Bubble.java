package cn.wgx.study.algorithm;

import java.util.Arrays;

/**
 *  冒泡排序
 *
 *  问题: 如何优化为 O(n)
 */
public class Bubble  extends SortBase {

    public static void main(String[] args) {
        int[] array = generate(100, 100);
        int[] array2 = Arrays.copyOf(array, array.length);
        print(array);

//        print(select(array));
        print(bubble(array));
        Arrays.sort(array2);

        print(array2);
        System.out.println(compare(array, array2));
    }


    public static int[] bubble(int array[]) {
        for (int i = 0, n = array.length; i < n-1; i++) {
            for (int j = 0; j < (n - i - 1); j++) {
                if(array[j] > array[j+1]){
                    exchange(array, j+1, j);
                }
            }
        }
        return array;
    }

}
