package cn.wgx.study.jdk8new;

import org.assertj.core.util.Arrays;

import java.util.List;

public class TestLamda {

    public static void main(String[] args) {
        String [] s = {"1","2","3","4","5","6","7","8","9","10","11"};

        List list = Arrays.asList(s);

        list.forEach(System.out::println);

    }

    public void m(){

        new Thread(new TestLamda()::run);

    }

    public void run(){


    }

}
