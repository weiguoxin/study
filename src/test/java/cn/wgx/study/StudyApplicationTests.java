package cn.wgx.study;

import cn.wgx.study.entity.Country;
import cn.wgx.study.repository.CountryRepositry;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest

public class StudyApplicationTests {

	Logger logger = LoggerFactory.getLogger(StudyApplicationTests.class);

	@Autowired
	CountryRepositry countryRepositry;


	@Before
	public void createEntity(){
		Country country = new Country();
		country.setCountryName("China");
		country.setCountryCode("+86");
		countryRepositry.save(country);
		assert country.getId() > 0 : "create error";
	}

	@Before
	public void restTemplate(){
		restTemplate = new RestTemplate();
	}

	@Test
	public void contextLoads() {
		List<Country> countries = countryRepositry.findAll();
		assert countries != null : "getdata is null";
		for(Country country: countries){
			logger.info("====== countryName = {}", country.getCountryName());
		}
	}

	private RestTemplate restTemplate;
	@Test
	public void testRestTemplate(){
		Object o  = restTemplate.getForObject("http://bjcg.sxycy.cn:82/CGCenter/api/fileupload/ueditor?action=config", String.class);
		System.out.println(o);
	}
}
