package cn.wgx.study.mybatis.dao;

import cn.wgx.study.mybatis.entity.User;

public interface UserMapper {

    //插入一个学生的信息,方法名要与UserMapper.xml中的insert标签的id相一致
    void insertUser(User user);


    //查询某个学生的信息,方法名要与UserMapper.xml中的select标签的id相一致
    User getUserById(Integer id);

}
