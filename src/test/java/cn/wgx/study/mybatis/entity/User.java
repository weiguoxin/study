package cn.wgx.study.mybatis.entity;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;
    private String username;
    private String sex;
    private Date birthday;
    private String address;
}
