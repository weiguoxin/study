package cn.wgx.study.juc;

import java.util.concurrent.CountDownLatch;

/**
 * CountDownLatch 线程倒数器
 * 1.通过初始化数量,
 * 2.每执行一次countDown()表示减一,
 * 3.直到计数为0再执行await()后续代码
 */
public class TestCountDownLatch {

    public static void main(String[] args) {
        testJoin();
    }


    public static void testJoin(){
        Thread[] threads = new Thread[1000];

        //初始化并设置数量
        CountDownLatch countDownLatch = new CountDownLatch(threads.length);

        for(int n=threads.length, i=0; i<n ;i++){
            threads[i] = new Thread(() -> {
                for(int r = 0, j = 0; j< 100000000; j ++){
                    r += j;
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //线程工作完,倒数一次
                countDownLatch.countDown();
            });
        }

        for(int n=threads.length, i=0; i<n ;i++){
            threads[i].start();
        }

        System.out.println("all start !");
        long start = System.currentTimeMillis();
        try {
            //等待倒数为0时,再继续执行后续代码
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("end!" + (end - start));

    }
}
