package cn.wgx.study.juc;

public class TestSynchronized {

    public static void main(String[] args) {

        TestSynchronized testSynchronized = new TestSynchronized();
        TestSynchronized testSynchronized2 = new TestSynchronized();

        new Thread(() -> {
            testSynchronized.m1(1);
//            testSynchronized.m2(1);
        }).start();


        testSynchronized2.m1(2);
//        testSynchronized.m2(2);

    }

    public synchronized void m1 (int n){
        System.out.println("Thread: " + n + " m1 start");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread: " + n + " m1 end");
    }

    public void m2 (int n){
        System.out.println("Thread: " +  n + " m2 start");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread: " + n + " m2 end");
    }

    public static synchronized void m3 (int n){
        System.out.println("Thread: " + n + " m3 start");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread: " + n + " m3 end");
    }


}
