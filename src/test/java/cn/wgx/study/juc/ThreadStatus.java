package cn.wgx.study.juc;

public class ThreadStatus implements Runnable{

    static Object o = new Object();


    public static void main(String[] args) {

        Thread thread = new Thread(new ThreadStatus());

        System.out.println(thread.getState());

        thread.start();

        System.out.println(thread.getState());

        try {
            Thread.sleep(5);
            System.out.println(thread.getState());
            Thread.sleep(4);
            System.out.println(thread.getState());
            synchronized (o){
                System.out.println(thread.getState());
                Thread.sleep(200);
                o.notify();
                System.out.println(thread.getState());
                thread.stop();
                System.out.println(thread.getState());
            }
            Thread.sleep(20);
            System.out.println(thread.getState());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run(){
        synchronized (o){
            try {
                Thread.sleep(10);
                o.notify();
//                int n = 1;
//                while(n<100000){n++;}
                o.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            o.notify();
        }
//        while(true){}

    }


}
