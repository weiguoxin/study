package cn.wgx.study.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestReentrantLock {

    public static void main(String[] args) throws InterruptedException {
        TestReentrantLock testReentrantLock = new TestReentrantLock();
        new Thread(testReentrantLock::m1).start();

        TimeUnit.SECONDS.sleep(1);

        new Thread(testReentrantLock::m2).start();

        Thread thread3 = new Thread(testReentrantLock::m3);
        thread3.start();
        TimeUnit.SECONDS.sleep(1);
        thread3.interrupt();
    }

    static ReentrantLock lock = new ReentrantLock();

    void m1() {
        try {
            lock.lock();
            for (int i = 0; i < 5; i++) {
                TimeUnit.SECONDS.sleep(1);
                System.out.println(i);
            }
        } catch (Exception e) {
        } finally {
            lock.unlock();
        }

    }

    void m2() {
        boolean b = false;
        try {
            b = lock.tryLock(1, TimeUnit.SECONDS);//尝试锁,超时后自动进入下面代码
            System.out.println("m2....." + b);
        } catch (Exception e) {
        } finally {
            if(b)lock.unlock();
        }
    }

    void m3() {
        try {
            lock.lockInterruptibly();//可被打断锁
            System.out.println("m3....." );
        } catch (Exception e) {
        } finally {
            if(lock.isHeldByCurrentThread())lock.unlock();
        }
    }


}
