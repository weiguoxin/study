package cn.wgx.study.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class TestReentrantLock2 {

    public static void main(String[] args) throws InterruptedException {
        TestReentrantLock2 testReentrantLock = new TestReentrantLock2();
        new Thread(testReentrantLock::m1).start();
        new Thread(testReentrantLock::m2).start();
        new Thread(testReentrantLock::m3).start();

    }

    static ReentrantLock lock = new ReentrantLock(true);

    void m1() {
        m();
    }

    void m2() {
        m();
    }

    void m3() {
        m();
    }

    void m(){
        for (int i = 0; i < 5; i++) {
            try {
                lock.lock();
                //TimeUnit.SECONDS.sleep(1);
                System.out.println(Thread.currentThread().getName() + " " + i);
            } catch (Exception e) {
            } finally {
                lock.unlock();
            }
        }
    }
}
