package cn.wgx.study.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TestReadWriteLock {

    /**
     * 读写锁
     * 读时不锁,写时锁
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        long s = System.currentTimeMillis();
//        Runnable read = () -> read(lock);
//        Runnable write = () -> write(lock);
        Runnable read = () -> read(readLock);
        Runnable write = () -> write(writeLock);
        for(int i=0;i<10;i++){
            Thread thread = new Thread(read);
            thread.start();
            //thread.join();
        }
        for(int i=0;i<10;i++){
            Thread thread = new Thread(write);
            thread.start();
            //thread.join();
        }

        long e = System.currentTimeMillis();
        System.out.println(e - s);
    }

    public static int value = 0;

    static  Lock lock = new ReentrantLock();
    static ReadWriteLock readWriteLock = new ReentrantReadWriteLock() ;
    static Lock readLock = readWriteLock.readLock();
    static Lock writeLock = readWriteLock.writeLock();

    public static void read(Lock lock){
        lock.lock();
        System.out.println("read value " + value);
        sleep(1000);
        lock.unlock();
    }

    public static void write(Lock lock){
        lock.lock();
        System.out.println("write value " + value);
        sleep(1000);
        lock.unlock();
    }

    public static void sleep(long time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
