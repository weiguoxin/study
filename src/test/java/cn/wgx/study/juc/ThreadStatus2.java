package cn.wgx.study.juc;

import java.util.concurrent.locks.LockSupport;

public class ThreadStatus2 implements Runnable{

    static Object o = new Object();


    public static void main(String[] args) {

        Thread thread = new Thread(new ThreadStatus2());

        System.out.println(thread.getState());

        thread.start();

        new Thread(()->{
            System.out.println(thread.getState());

            LockSupport.park(thread);

            System.out.println(thread.getState());

            LockSupport.unpark(thread);

            System.out.println(thread.getState());

            LockSupport.parkNanos(500);

            System.out.println(thread.getState());

            try {
                Thread.sleep(600);
                System.out.println(thread.getState());
                thread.stop();
                System.out.println(thread.getState());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();
        return;
    }

    public void run(){
        while(true){}
    }


}
