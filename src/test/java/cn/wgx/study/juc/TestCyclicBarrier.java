package cn.wgx.study.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * CyclicBarrier  线程凑数等待器
 * 1. 初始化数量
 * 2. 通过 await() 方法阻塞线程,等待足够数量
 * 3. 线程启动数量等于初始化数量后,先执行预定义方法(可选),再通知所有线程继续执行
 *
 */
public class TestCyclicBarrier {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(100, () -> {
            System.out.println("人够了");
        });

        for(int i=0; i < 100; i++){
            new Thread(() -> {
                String name = Thread.currentThread().getName();
                try {
                    System.out.println("线程: " + name + " 准备好了");
                    cyclicBarrier.await(3, TimeUnit.SECONDS);
                    System.out.println("线程: " + name + " 开始执行--------");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    System.out.println("BrokenBarrierException : " + name);
                } catch (TimeoutException e) {
                    System.out.println("超时错误 : "+ name);
                }
            }, "" + i).start();
        }
        System.out.println("end");
    }

}
