package cn.wgx.study.repository;

import cn.wgx.study.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepositry extends JpaRepository<Country, Long> {
}
