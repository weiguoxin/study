package cn.wgx.study.entity;

import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name="country")
@Data
public class Country {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String countryName;
    private String countryCode;

}
