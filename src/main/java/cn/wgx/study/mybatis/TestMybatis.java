package cn.wgx.study.mybatis;

import cn.wgx.study.mybatis.dao.UserMapper;
import cn.wgx.study.mybatis.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
public class TestMybatis {

    public static void main(String[] args) throws IOException {

        InputStream resourceAsStream = Resources.getResourceAsStream("mybatis.xml");

        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(resourceAsStream);

        SqlSession sqlSession = factory.openSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
//        for(int i=0;i<10;i++){
//            User user = new User();
//            user.setUsername("小王" + i);
//            user.setSex(i+"");
//            mapper.insertUser(user);
//        }
        sqlSession.commit();
        Object o = sqlSession.selectOne("cn.wgx.study.mybatis.dao.UserMapper.getUserById", 61);
        log.info("user:{}", o);

        sqlSession.close();

    }

}
