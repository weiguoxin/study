package cn.wgx.study.web;

import cn.wgx.study.entity.Country;
import cn.wgx.study.rabbitmq.MQProducer;
import cn.wgx.study.repository.CountryRepositry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller

public class CountryController {

    @Autowired
    CountryRepositry countryRepositry;

    @Autowired
    MQProducer mqProducer;

    @GetMapping(value = "/")
    public String get(ModelMap modelMap){
        List<Country> countries = countryRepositry.findAll();
        modelMap.put("c", countries);
        return "index";
    }

    @GetMapping(value = "/mq")
    public String mq(String msg){
        if(null == msg ){msg = "test";}
        mqProducer.sendMsg(msg);
        return "index";
    }
}
